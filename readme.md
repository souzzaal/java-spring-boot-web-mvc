# Spring Boot MVC Web

Projeto para estudo de aplicação Web MVC usando Spring Boot.

A aplicação permite a listagem (ordenada pela prioridade), cadastro e exclusão (em memória, sem uso banco de dados) de Atividades.

## Dependências

- Java 14
- spring-boot-starter-web
- spring-boot-starter-validation
- spring-boot-starter-thymeleaf (view engine)

## Estrutura do projeto

- Models = src/main/java/br/com/souzzaal/springwebmvc/models
- Controllers = src/main/java/br/com/souzzaal/springwebmvc/controllers
- Views = src/main/resources/templates
- Repositories = src/main/java/br/com/souzzaal/springwebmvc/repositories





