package br.com.souzzaal.springwebmvc.controllers;

import br.com.souzzaal.springwebmvc.models.Atividade;
import br.com.souzzaal.springwebmvc.repositories.AtividadeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller // informa ao framework que esta classe eh um controller e que ele deve gerenciar o seu ciclo de vida.
public class AtividadeController {

    /*
        Aplicando a injecao de dependencias do Spring: Ao instanciar a classe, automaticamente ele criara
        uma instanncia da clase JediRepositury na variavel repository.
    */
    @Autowired
    private AtividadeRepository repository;

    @GetMapping("/atividades") // informa ao framework para executar esse metodo quando receber uma requisicao GET "/atividades"
    public ModelAndView atividades() {

        final var modelAndView = new ModelAndView();
        modelAndView.setViewName("atividade"); // nome da view que sera exibida
        modelAndView.addObject("atividades", repository.getAtividades()); // disponibiliza um objeto para manipulacao na pagina que estara acessivel via chave ("atividades", neste caso)
        return modelAndView;
    }

    @GetMapping("/nova-atividade")
    public ModelAndView novaAtividade() {
        final var modelAndView = new ModelAndView();
        modelAndView.setViewName("nova-atividade");
        modelAndView.addObject("atividade", new Atividade());

        return modelAndView;
    }

    @PostMapping("/nova-atividade")
    public String criarAtividade(@Valid @ModelAttribute Atividade atividade, BindingResult result, RedirectAttributes redirectAttributes) {
        //@Valid adiciona verificacao de validacao dos campos (regras de validacao estao no model)
        // @ModelAtribute criara um objeto do tipo Jedi com as informacoes vindas do formaulario (name, e lastName)

        if(result.hasErrors()) {
            return "nova-atividade";
        }

        repository.add(atividade);
        redirectAttributes.addFlashAttribute("message", "Atividade cadastrado com sucesso!");

        return "redirect:atividades"; // redireciona para "/atividades"
    }

    @GetMapping("/excluir-atividade")
    public String excluirAtividade(@RequestParam int codigo) {
        repository.remove(codigo);

        return "redirect:atividades";
    }
}
