package br.com.souzzaal.springwebmvc.models;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

public class Atividade {

    @NotBlank(message = "O campo nome é obrigatário") // regra de validação
    private String nome;

    @NotNull(message = "O campo prioridade nome é obrigatário")
    @PositiveOrZero(message = "O campo prioridade dever maior ou igual a zero")
    private int prioridade;

    private int codigo;

    public Atividade() {
    }

    public Atividade(String nome, int prioridade, int codigo) {
        this.nome = nome;
        this.prioridade = prioridade;
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getPrioridade() {
        return prioridade;
    }

    public void setPrioridade(int prioridade) {
        this.prioridade = prioridade;
    }
}
