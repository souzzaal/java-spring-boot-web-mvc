package br.com.souzzaal.springwebmvc.repositories;

import br.com.souzzaal.springwebmvc.models.Atividade;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository // permite a injecao de dependencia automatica do Spring
public class AtividadeRepository {

    private Map<Integer, Atividade> atividadesDB;

    public AtividadeRepository() {
        atividadesDB = new HashMap<>();
        atividadesDB.put(1, new Atividade("Exemplo", 0, atividadesDB.size()+1));
    }

    public List<Atividade> getAtividades()
    {
        Collection<Atividade> atividadeCollection = this.atividadesDB.values();
        var atividades = new ArrayList<Atividade>(atividadeCollection);
        atividades.sort(Comparator.comparingInt(Atividade::getPrioridade));

        return atividades;
    }

    public void add(Atividade atividade) {
        atividade.setCodigo(atividadesDB.size()+1);
        this.atividadesDB.put(atividade.getCodigo(), atividade);
    }

    public void remove(int codigo) {
        /*
            Pode-se omitir o "this." quando nao houver ambiguidade, neste caso considera-se a existencia implicita do
             "this." para acessar o atributo da classe
        */
        atividadesDB.remove(codigo);
    }
}
